﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using AppAproaciones;

namespace AppAproaciones.Controllers
{
    public class SOLICITUDES_AUTOController : Controller
    {
        private EXACTUSEntities db = new EXACTUSEntities();

        private string connetionString = "Data Source=192.168.7.2;Initial Catalog=EXACTUS;Persist Security Info=True;User ID=sa;Password=jda";

        // GET: SOLICITUDES_AUTO
        public ActionResult Index()
        {
            
            return View(db.SOLICITUDES_AUTO.ToList());
        }

        // GET: SOLICITUDES_AUTO/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SOLICITUDES_AUTO sOLICITUDES_AUTO = db.SOLICITUDES_AUTO.Find(id);
            if (sOLICITUDES_AUTO == null)
            {
                return HttpNotFound();
            }
          
            return View(sOLICITUDES_AUTO);
        }
        public ActionResult Aprobar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SOLICITUDES_AUTO sOLICITUDES_AUTO = db.SOLICITUDES_AUTO.Find(id);
            if (sOLICITUDES_AUTO == null)
            {
                return HttpNotFound();
            }
            sOLICITUDES_AUTO.ESTADO = "A";
            db.SaveChanges();
            TempData["msg"] = "<script>alert('Se Aprobo con Exito!!!');</script>";
            string Accion = "Aprobada";
            EniaviarCorreo(sOLICITUDES_AUTO, Accion);
            ActivarJob(sOLICITUDES_AUTO);
            return RedirectToAction("Details/" + id.ToString());
        }

        public ActionResult Rechazar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SOLICITUDES_AUTO sOLICITUDES_AUTO = db.SOLICITUDES_AUTO.Find(id);
            if (sOLICITUDES_AUTO == null)
            {
                return HttpNotFound();
            }

            sOLICITUDES_AUTO.ESTADO = "R";
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public void EniaviarCorreo(SOLICITUDES_AUTO sOLICITUDES_AUTO,string Accion)
        {
          
            SqlConnection cnn;
          
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            string query = "SELECT CORREO_ELECTRONICO FROM erpadmin.USUARIO WHERE USUARIO= '"+sOLICITUDES_AUTO.USUARIO_SOLICITUD.ToString()+"'";
          
            SqlCommand cmd3 = new SqlCommand(query, cnn);
            var quer = cmd3.ExecuteScalar();
            
            if (quer== null)
            {
            }
            else
            {
                string correo = quer.ToString();
                correo = (string)cmd3.ExecuteScalar();
                var fromAddress = new MailAddress("brayan.reyes@emasal.com", "From Name");
                //Aqui un if compañaia
                var toAddress = new MailAddress(correo, "To Name");
                const string fromPassword = "Brayan@2019$";
                const string subject = "Solicitudes Exactus";
                string body = "La solicitud N# " + sOLICITUDES_AUTO.IDSOLICITUES_AUTO.ToString() + " que contiene el articulo: " + sOLICITUDES_AUTO.ARTICULO.ToString() + " Ha sido " + Accion.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            }

        }

        public void ActivarJob(SOLICITUDES_AUTO sOLICITUDES_AUTO)
        {
            int IdJob=0;
            switch (sOLICITUDES_AUTO.COMPANIA)
            {
                case "EMASAL":
                    IdJob = 12;
                    break;
                case "EMASALCR":
                    IdJob = 15;
                    break;
                case "EMASALGT":
                    IdJob = 16;
                    break;
                case "EMASALHN":
                    IdJob = 17;
                    break;
                case "EMASALNI":
                    IdJob = 18;
                    break;
                case "EMASALPA":
                    IdJob = 19;
                    break;
                case "EMASALPTY":
                    IdJob = 20;
                    break;

            }
          
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            cnn = new SqlConnection(connetionString);
            DateTime currentTime = DateTime.Now;
            //Aqui se setea el tiempo de cierre del parametro
            DateTime x30MinsLater = currentTime.AddMinutes(30);
            string TimeAc = x30MinsLater.ToString("HH:mm:ss");
            TimeAc = TimeAc.Replace(":","");
            string Fecha= DateTime.Now.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            cnn.Open();
            string sql2 = "EXEC msdb.dbo.sp_update_schedule @schedule_id="+IdJob.ToString()+", @active_start_time ="+ TimeAc + ",@enabled = 1 ,@active_start_date = "+Fecha+"";
            SqlCommand cmd2 = new SqlCommand(sql2, cnn);
            cmd2.ExecuteNonQuery();
            string sql3 = "UPDATE "+sOLICITUDES_AUTO.COMPANIA.ToString()+".globales_fa SET PRECIO_COSTO='S'";
            SqlCommand cmd3 = new SqlCommand(sql3, cnn);
            cmd3.ExecuteNonQuery();




        }

        // GET: SOLICITUDES_AUTO/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SOLICITUDES_AUTO/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDSOLICITUES_AUTO,COMPANIA,USUARIO_SOLICITUD,FECHA_HORA_CREACION,ARTICULO,RAZON_SOLICITUD,ESTADO,FECHA_HORA_APORBACION,USUARIO_APROBACION,RAZON_RECHAZADA,DescripcionArticulo")] SOLICITUDES_AUTO sOLICITUDES_AUTO)
        {
            if (ModelState.IsValid)
            {
                db.SOLICITUDES_AUTO.Add(sOLICITUDES_AUTO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sOLICITUDES_AUTO);
        }

        // GET: SOLICITUDES_AUTO/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SOLICITUDES_AUTO sOLICITUDES_AUTO = db.SOLICITUDES_AUTO.Find(id);
            if (sOLICITUDES_AUTO == null)
            {
                return HttpNotFound();
            }
            sOLICITUDES_AUTO.ESTADO = "R";
            db.SaveChanges();
              
            return View(sOLICITUDES_AUTO);
        }

        // POST: SOLICITUDES_AUTO/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDSOLICITUES_AUTO,COMPANIA,USUARIO_SOLICITUD,FECHA_HORA_CREACION,ARTICULO,RAZON_SOLICITUD,ESTADO,FECHA_HORA_APORBACION,USUARIO_APROBACION,RAZON_RECHAZADA,DescripcionArticulo")] SOLICITUDES_AUTO sOLICITUDES_AUTO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sOLICITUDES_AUTO).State = EntityState.Modified;
                db.SaveChanges();
                TempData["msg"] = "<script>alert('Se Rechazo con Exito!!!');</script>";
                string Accion = "Rechazada";
                EniaviarCorreo(sOLICITUDES_AUTO, Accion);
                return RedirectToAction("Details/" + sOLICITUDES_AUTO.IDSOLICITUES_AUTO.ToString());
            }
            return View(sOLICITUDES_AUTO);
        }

        // GET: SOLICITUDES_AUTO/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SOLICITUDES_AUTO sOLICITUDES_AUTO = db.SOLICITUDES_AUTO.Find(id);
            if (sOLICITUDES_AUTO == null)
            {
                return HttpNotFound();
            }
            return View(sOLICITUDES_AUTO);
        }

        // POST: SOLICITUDES_AUTO/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SOLICITUDES_AUTO sOLICITUDES_AUTO = db.SOLICITUDES_AUTO.Find(id);
            db.SOLICITUDES_AUTO.Remove(sOLICITUDES_AUTO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
